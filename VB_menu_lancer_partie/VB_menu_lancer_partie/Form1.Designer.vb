﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Frm_lancer_partie
    Inherits System.Windows.Forms.Form

    'Form remplace la méthode Dispose pour nettoyer la liste des composants.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requise par le Concepteur Windows Form
    Private components As System.ComponentModel.IContainer

    'REMARQUE : la procédure suivante est requise par le Concepteur Windows Form
    'Elle peut être modifiée à l'aide du Concepteur Windows Form.  
    'Ne la modifiez pas à l'aide de l'éditeur de code.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.lbl_titre = New System.Windows.Forms.Label()
        Me.lbl_nb_joueur = New System.Windows.Forms.Label()
        Me.lbl_nom_j1 = New System.Windows.Forms.Label()
        Me.lbl_nom_j2 = New System.Windows.Forms.Label()
        Me.lbl_nom_j4 = New System.Windows.Forms.Label()
        Me.lbl_nom_j3 = New System.Windows.Forms.Label()
        Me.cbo_nb_joueur = New System.Windows.Forms.ComboBox()
        Me.cmd_jouer = New System.Windows.Forms.Button()
        Me.cmd_aide = New System.Windows.Forms.Button()
        Me.txt_j1 = New System.Windows.Forms.TextBox()
        Me.txt_j2 = New System.Windows.Forms.TextBox()
        Me.txt_j4 = New System.Windows.Forms.TextBox()
        Me.txt_j3 = New System.Windows.Forms.TextBox()
        Me.SuspendLayout()
        '
        'lbl_titre
        '
        Me.lbl_titre.AutoSize = True
        Me.lbl_titre.Font = New System.Drawing.Font("Microsoft Sans Serif", 25.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_titre.Location = New System.Drawing.Point(155, 22)
        Me.lbl_titre.Name = "lbl_titre"
        Me.lbl_titre.Size = New System.Drawing.Size(459, 48)
        Me.lbl_titre.TabIndex = 0
        Me.lbl_titre.Text = "Bienvenu sur le Qwirkle"
        '
        'lbl_nb_joueur
        '
        Me.lbl_nb_joueur.AutoSize = True
        Me.lbl_nb_joueur.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_nb_joueur.Location = New System.Drawing.Point(56, 135)
        Me.lbl_nb_joueur.Name = "lbl_nb_joueur"
        Me.lbl_nb_joueur.Size = New System.Drawing.Size(242, 29)
        Me.lbl_nb_joueur.TabIndex = 1
        Me.lbl_nb_joueur.Text = "Nombre de joueurs :"
        '
        'lbl_nom_j1
        '
        Me.lbl_nom_j1.AutoSize = True
        Me.lbl_nom_j1.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_nom_j1.Location = New System.Drawing.Point(57, 211)
        Me.lbl_nom_j1.Name = "lbl_nom_j1"
        Me.lbl_nom_j1.Size = New System.Drawing.Size(119, 20)
        Me.lbl_nom_j1.TabIndex = 2
        Me.lbl_nom_j1.Text = "Nom joueur 1 :"
        '
        'lbl_nom_j2
        '
        Me.lbl_nom_j2.AutoSize = True
        Me.lbl_nom_j2.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_nom_j2.Location = New System.Drawing.Point(428, 211)
        Me.lbl_nom_j2.Name = "lbl_nom_j2"
        Me.lbl_nom_j2.Size = New System.Drawing.Size(119, 20)
        Me.lbl_nom_j2.TabIndex = 3
        Me.lbl_nom_j2.Text = "Nom joueur 2 :"
        '
        'lbl_nom_j4
        '
        Me.lbl_nom_j4.AutoSize = True
        Me.lbl_nom_j4.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_nom_j4.Location = New System.Drawing.Point(428, 283)
        Me.lbl_nom_j4.Name = "lbl_nom_j4"
        Me.lbl_nom_j4.Size = New System.Drawing.Size(119, 20)
        Me.lbl_nom_j4.TabIndex = 4
        Me.lbl_nom_j4.Text = "Nom joueur 4 :"
        '
        'lbl_nom_j3
        '
        Me.lbl_nom_j3.AutoSize = True
        Me.lbl_nom_j3.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_nom_j3.Location = New System.Drawing.Point(57, 281)
        Me.lbl_nom_j3.Name = "lbl_nom_j3"
        Me.lbl_nom_j3.Size = New System.Drawing.Size(119, 20)
        Me.lbl_nom_j3.TabIndex = 5
        Me.lbl_nom_j3.Text = "Nom joueur 3 :"
        '
        'cbo_nb_joueur
        '
        Me.cbo_nb_joueur.FormattingEnabled = True
        Me.cbo_nb_joueur.Items.AddRange(New Object() {"2 joueurs", "3 joueurs", "4 joueurs"})
        Me.cbo_nb_joueur.Location = New System.Drawing.Point(348, 138)
        Me.cbo_nb_joueur.Name = "cbo_nb_joueur"
        Me.cbo_nb_joueur.Size = New System.Drawing.Size(240, 24)
        Me.cbo_nb_joueur.TabIndex = 6
        '
        'cmd_jouer
        '
        Me.cmd_jouer.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmd_jouer.Location = New System.Drawing.Point(227, 365)
        Me.cmd_jouer.Name = "cmd_jouer"
        Me.cmd_jouer.Size = New System.Drawing.Size(320, 63)
        Me.cmd_jouer.TabIndex = 7
        Me.cmd_jouer.Text = "Jouer "
        Me.cmd_jouer.UseVisualStyleBackColor = True
        '
        'cmd_aide
        '
        Me.cmd_aide.Location = New System.Drawing.Point(12, 390)
        Me.cmd_aide.Name = "cmd_aide"
        Me.cmd_aide.Size = New System.Drawing.Size(51, 38)
        Me.cmd_aide.TabIndex = 8
        Me.cmd_aide.Text = "Aide"
        Me.cmd_aide.UseVisualStyleBackColor = True
        '
        'txt_j1
        '
        Me.txt_j1.Location = New System.Drawing.Point(182, 209)
        Me.txt_j1.Name = "txt_j1"
        Me.txt_j1.Size = New System.Drawing.Size(146, 22)
        Me.txt_j1.TabIndex = 9
        '
        'txt_j2
        '
        Me.txt_j2.Location = New System.Drawing.Point(553, 209)
        Me.txt_j2.Name = "txt_j2"
        Me.txt_j2.Size = New System.Drawing.Size(146, 22)
        Me.txt_j2.TabIndex = 10
        '
        'txt_j4
        '
        Me.txt_j4.Location = New System.Drawing.Point(553, 283)
        Me.txt_j4.Name = "txt_j4"
        Me.txt_j4.Size = New System.Drawing.Size(146, 22)
        Me.txt_j4.TabIndex = 11
        '
        'txt_j3
        '
        Me.txt_j3.Location = New System.Drawing.Point(182, 279)
        Me.txt_j3.Name = "txt_j3"
        Me.txt_j3.Size = New System.Drawing.Size(146, 22)
        Me.txt_j3.TabIndex = 12
        '
        'Frm_lancer_partie
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 16.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(800, 450)
        Me.Controls.Add(Me.txt_j3)
        Me.Controls.Add(Me.txt_j4)
        Me.Controls.Add(Me.txt_j2)
        Me.Controls.Add(Me.txt_j1)
        Me.Controls.Add(Me.cmd_aide)
        Me.Controls.Add(Me.cmd_jouer)
        Me.Controls.Add(Me.cbo_nb_joueur)
        Me.Controls.Add(Me.lbl_nom_j3)
        Me.Controls.Add(Me.lbl_nom_j4)
        Me.Controls.Add(Me.lbl_nom_j2)
        Me.Controls.Add(Me.lbl_nom_j1)
        Me.Controls.Add(Me.lbl_nb_joueur)
        Me.Controls.Add(Me.lbl_titre)
        Me.Name = "Frm_lancer_partie"
        Me.Text = "Form1"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents lbl_titre As Label
    Friend WithEvents lbl_nb_joueur As Label
    Friend WithEvents lbl_nom_j1 As Label
    Friend WithEvents lbl_nom_j2 As Label
    Friend WithEvents lbl_nom_j4 As Label
    Friend WithEvents lbl_nom_j3 As Label
    Friend WithEvents cbo_nb_joueur As ComboBox
    Friend WithEvents cmd_jouer As Button
    Friend WithEvents cmd_aide As Button
    Friend WithEvents txt_j1 As TextBox
    Friend WithEvents txt_j2 As TextBox
    Friend WithEvents txt_j4 As TextBox
    Friend WithEvents txt_j3 As TextBox
End Class
